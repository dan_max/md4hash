#ifndef MD4_h
#define MD4_h
#define uc unsigned char


#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <stdint.h>
#include <iterator>
using namespace std;


class MD4{
private:
    string input = "";
    uint64_t lenght;
    uint32_t a, b, c, d;
    vector<uc> read(string in);
    void write(string out, vector<uc> in);
    vector<uc> addition(vector<uc> in);
    vector<uc> LenghttoByte(uint64_t len);
    vector<uc> toByte(uint32_t in);
    void init();
    uint32_t convert(vector<uc> in);  
    void out(uint32_t in);
    void hex(uc in);
    uint32_t f(uint32_t x, uint32_t y, uint32_t z);
    uint32_t g(uint32_t x, uint32_t y, uint32_t z);
    uint32_t h(uint32_t x, uint32_t y, uint32_t z);
    uint32_t rotateleft(uint32_t x, int count);
public:
    MD4(string in);
    ~MD4();
    void test();
    void hash();
    
};

#endif //MD4_h
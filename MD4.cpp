#include "MD4.h"

MD4::MD4(string in){
    MD4::input = in;
}

vector<uc> MD4::read(string in){
    FILE *input;
    if ((input=fopen(in.c_str(), "rb"))==NULL) {
        printf ("Cannot open file.\n");
    }
    vector<uc> res;
    uc t;
    while(!feof(input)){
        fread(&t,sizeof(t),1,input);
        res.push_back(t);
    }
    res.pop_back();
    fclose(input);
    return res;
}

void MD4::write(string out, vector<uc> in){
    FILE *output;
    if ((output=fopen(out.c_str(), "wb"))==NULL) {
        printf ("Cannot open file.\n");
        return;
    }
    uc t;
    for(int i = 0; i < in.size(); ++i){
        t = in[i];
        fwrite(&t,sizeof(t),1,output);
    }
    fclose(output);
}

vector<uc> MD4::LenghttoByte(uint64_t len){
    vector<uc> res(8);
    for(int i = 0; i < 8; ++i){
        res[7 - i] = (len >> (i * 8));
    }
    return res;
}

vector<uc> MD4::addition(vector<uc> in){
    //add bits ro size % 512 = 448 in bits
    bool is_first = true;
    int add = ((56 - in.size() % 64) + 64) % 64;
    vector<uc> res(in);
    if(add == 0){
        add = 64;
    }
    for(int i = 0; i < add; ++i){
        if(is_first){
            res.push_back(128);
            is_first = false;
        }
        else{
            res.push_back(0);
        }
    }
    vector<uc> l = LenghttoByte(MD4::lenght);
    for(int i = 4; i < l.size(); i++){
        res.push_back(l[i]);
    }
    for(int i = 0; i < l.size() / 2; i++){
        res.push_back(l[i]);
    }
    return res;
}

void MD4::init(){
    a = 1732584193;
    b = 4023233417;
    c = 2562383070;
    d = 271733878;
}

uint32_t MD4::convert(vector<uc> in) {
    uint32_t res = 0;
    for(int i = 0; i < 4; ++i){
        res = res << 8;
        res += in[i]; 
    }
    return res;
}

vector<uc> MD4::toByte(uint32_t in){
    vector<uc> res(4);
    for(int i = 0; i < 4; ++i){
        res[3 - i] = (in >> (i * 8));
    }
    return res;
}

void MD4::hex(uc in){
    uc first = in / (1 << 4), second = in % (1 << 4);
    if(first < 10){
        cout << (char)(first + '0');
    }
    else{
        cout << (char)(first - 10 + 'A');
    }
    if(second < 10){
        cout << (char)(second + '0');
    }
    else{
        cout << (char)(second - 10 + 'A');
    } 
}

void MD4::out(uint32_t in){
    vector<uc> t = toByte(in);
    for(int i = 0; i < t.size(); i++){
        hex(t[i]);
        cout << " ";
    }
}

uint32_t MD4::f(uint32_t x, uint32_t y, uint32_t z){
    return ((x & y) | ((!x) & z));
}

uint32_t MD4::g(uint32_t x, uint32_t y, uint32_t z){
    return ((x & y) | (x & z) | (y & z));
}

uint32_t MD4::h(uint32_t x, uint32_t y, uint32_t z){
    return x ^ y ^ z;
}

uint32_t MD4::rotateleft(uint32_t x, int count){
    return (x << count) | (x >> (32 - count));
}

void MD4::hash(){
    vector<uc> in = read(MD4::input);
    cout<<"~~~in"<<endl;
    MD4::lenght = in.size();
    cout << in.size() << endl;
    for(int i = 0; i < in.size(); i++){
        hex(in[i]);
        cout << " ";
    }
    cout<<endl;
    cout<<"~~~~~"<<endl;
    vector<uc> t = addition(in);
    in.clear();
    cout<<"~~add"<<endl;
    cout << t.size() << endl;
    for(int i = 0; i < t.size(); i++){
        hex(t[i]);
        cout << " ";
    }
    cout<<endl;
    cout<<"~~~~~"<<endl;
    vector<uint32_t> res;
    vector<uc> temp;
    for(int i= 0; i < t.size() / 4; i++){
        temp.push_back(t[i * 4 + 0]);
        temp.push_back(t[i * 4 + 1]);
        temp.push_back(t[i * 4 + 2]);
        temp.push_back(t[i * 4 + 3]);
        res.push_back(convert(temp));
        temp.clear();
    }
    t.clear();
    cout<<"words"<<endl;
    cout << res.size() << endl;
    for(int i = 0; i < res.size(); i++){
        out(res[i]);
        cout << endl;
    }
    cout<<endl;
    cout<<"~~~~~"<<endl;
    init();
    vector<uint32_t> x;
    uint32_t aa, bb, cc, dd;
    int i, j;
    for(i = 0; i < res.size() / 16; i++){
//        cout <<"~~~~x"<< endl;
        for(j = 0; j < 16; j++){
            x.push_back(res[i * 16 + j]);
//            out(x[j]);
//            cout << endl;
        }
//        cout <<"~~~~~"<< endl;
        aa = a;
        bb = b;
        cc = c;
        dd = d;
        //round 1
        for(j = 0; j < 4; j++){
            cout<< j*4 << " " << j * 4 + 1 << " " << j * 4 + 2 << " " << j * 4 + 3 << endl;
            a = rotateleft(a + f(b,c,d) + x[j * 4], 3);
            d = rotateleft(d + f(a,b,c) + x[j * 4 + 1], 7);
            c = rotateleft(c + f(d,a,b) + x[j * 4 + 2], 11);
            b = rotateleft(b + f(c,d,a) + x[j * 4 + 3], 19);
        }
        //round 2
        for(j = 0; j < 4; j++){
            cout<< j << " " << j + 4 << " " << j + 8 << " " << j + 12<< endl;
            a = rotateleft(a + g(b,c,d) + x[j] + 1518500249, 3);
            d = rotateleft(d + g(a,b,c) + x[j + 4] + 1518500249, 5);
            c = rotateleft(c + g(d,a,b) + x[j + 8] + 1518500249, 9);
            b = rotateleft(b + g(c,d,a) + x[j + 12] + 1518500249, 13);
        }        
        //round 3
        int ind_a = 0, ind_b = 12, ind_c = 4, ind_d = 8;
        for(j = 0; j < 4; j++){
            cout<< ind_a << " " << ind_d << " " << ind_c << " " << ind_b << endl;
            a = rotateleft(a + h(b,c,d) + x[ind_a] + 1859775393, 3);
            d = rotateleft(d + h(a,b,c) + x[ind_d] + 1859775393, 9);
            c = rotateleft(c + h(d,a,b) + x[ind_c] + 1859775393, 11);
            b = rotateleft(b + h(c,d,a) + x[ind_b] + 1859775393, 15);
            if(j % 2 == 0){
                ind_a += 2;
                ind_b += 2;
                ind_c += 2;
                ind_d += 2;
            }
            else{
                ind_a--;
                ind_b--;
                ind_c--;
                ind_d--;
            } 
        }
        x.clear();
        a = a + aa;
        b = b + bb;
        c = c + cc;
        d = d + dd;
    }
    
    cout << "hash:";
    out(a);
    out(b);
    out(c);
    out(d);
    cout << endl;
}

void MD4::test(){
    hash();
}

MD4::~MD4(){}